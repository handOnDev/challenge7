const user = require('../models').user;
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

// error handling
const handleErrors = (err) => {
    console.log(err.message, err.code);
    let errors = { email: '', password: ''};

// login - incorrect email
    if(err.message === 'Incorrect email'){
        errors.email = 'That email is incorrect'
    }

    // login - incorrect password
    if(err.message === 'Incorrect password'){
        errors.password = 'That password is incorrect';
    }

    // signup - duplication errors
    if(err.code === 11000){
        errors['email'] = "Email already exist. Please input a new one."
        return errors;

    }
    // signup - validation errors
    if(err.message.includes('user validation failed')){
        Object.values(err.errors).forEach(({properties}) => {
            errors[properties.path] = properties.message;
        });
    }
    return errors;
}

// token
const maxAge = 3 * 24 * 60 * 60 //3dx24hx60mx60s
const createToken = (id) => {
    const secret = 'String Rahasia BINAR'
    return jwt.sign({id}, secret , {
        expiresIn: maxAge
    })
}

//HTTP endpoints

// HTTP get / endpoint
module.exports.index_get =  (req, res)=> {
    // const {username, password} = req.body;

    try{
        res.render('index');
    }catch(err){
        console.log(err);
        res.status(500).json(err);
    }
};

// HTTP get /register endpoint
module.exports.register_get =  (req, res)=> {
    // const {username, password} = req.body;

    try{
        res.render('register');
    }catch(err){
        console.log(err);
        res.status(500).json(err);
    }
};

// HTTP get /dashboard endpoint
module.exports.dashboard_get =  (req, res, next)=> {
    user.findAll()
    .then((users) =>{
        res.render('dashboard', {
            title:'Dashboard',
            data: users
        });
    })
    .catch(next);
}

    
// HTTP get /game endpoint
module.exports.game_get =  (req, res)=> {
    // const {username, password} = req.body;

    try{
        res.render('game');
    }catch(err){
        console.log(err);
        res.status(500).json(err);
    }
};

// HTTP get /LOGIN endpoint
module.exports.login_get =  (req, res)=> {
    // const {username, password} = req.body;

    try{
        res.render('login');
    }catch(err){
        console.log(err);
        res.status(500).json(err);
    }
};

//  HTTP post /register endpoint
module.exports.register_post = async (req, res)=> {
    // grab username & password from http body
    let {username, password} = req.body;
    
    // post to db, send cookie to client
    try{
        addUser = await user.create({username, password});
        const token = createToken(user._id);
        res.cookie('jwt', token, {
            httpOnly: true,
            maxAge: maxAge * 1000, //3dx24hx60mx60sx60ms
            secure: true
        });
        res.status(201).json({user: user._id});
        console.log('user created');
    }catch(err){
        console.log(err);
        res.status(400).json(err);
    }
};

//  HTTP post /login endpoint
module.exports.login_post = async (req, res)=> {
    // grab username & password from http body
    let {username, password} = req.body;
    
    // using static method
    try{
        user = await user.login({username, password});
        const token = createToken(user._id);
        res.cookie('jwt', token, {
            httpOnly: true,
            maxAge: maxAge * 1000, //3dx24hx60mx60sx60ms
            secure: true
        });
        res.status(200).json({user: user._id});
        // console.log('user logged in');
    }catch(err){
        const errors = handleErrors(err);
        res.status(400).json({errors});
    }
};

// HTTP get /update:id endpoint
module.exports.update_get =  (req, res)=> {
 
    try{
        user.findOne({
            where: {
                id: req.params.id
            }
        }).then((user)=>{
            res.render('login', {
                title: 'Update User',
                data: user
            });
        })
        
    }catch(err){
        console.log(err);
        res.status(500).json(err);
    }
};

//  HTTP post /update endpoint
module.exports.register_post = async (req, res)=> {
    // grab username & password from http body
    let {username, password} = req.body;
    
    // post to db, send cookie to client
    try{
        addUser = await user.create({username, password});
        const token = createToken(user._id);
        res.cookie('jwt', token, {
            httpOnly: true,
            maxAge: maxAge * 1000, //3dx24hx60mx60sx60ms
            secure: true
        });
        res.status(201).json({user: user._id});
        console.log('user created');
    }catch(err){
        console.log(err);
        res.status(400).json(err);
    }
};





