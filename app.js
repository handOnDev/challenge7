const express = require('express');
const app = express();
const PORT = 3000 || process.env.PORT;
let { sequelize, user } = require('./models')
const cookieParser = require('cookie-parser');
const appRouter = require('./routes/appRouter')

// middleware
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use('/css', express.static(__dirname + '/assets/css'))
app.use('/js', express.static(__dirname + '/assets/js'))
app.use('/img', express.static(__dirname + '/assets/img'))
app.use(cookieParser());
app.use(express.json());
app.use(appRouter);

// update database (for development only)
async function dbConn(){
    await sequelize.authenticate();
}



app.listen(PORT, () => {
    dbConn();
    console.log(`server running on port ${PORT}`)
})