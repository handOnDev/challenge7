class game {
    constructor(playerHand, cpuHand) {
        this.playerHand = playerHand
        this.cpuHand = cpuHand
    }

    rockGo() {
        // playerRock.style.backgroundColor = "rgba(255,255,255, 0.8)"
        // playerRock.style.borderRadius = "12px"
        playerRock.classList.replace("item-highlight", "active")
        this.CpuChoose()
        console.log("player adalah " + this.playerHand)
        console.log("cpu adalah " + this.cpuHand)
        this.playGame()
    }

    paperGo() {
        // playerPaper.style.backgroundColor = "rgba(255,255,255, 0.8)"
        // playerPaper.style.borderRadius = "12px"
        playerPaper.classList.replace("item-highlight", "active")
        this.CpuChoose()
        console.log("player adalah " + this.playerHand)
        console.log("cpu adalah " + this.cpuHand)
        this.playGame()
    }

    scissorsGo() {
        // playerScissors.style.backgroundColor = "rgba(255,255,255, 0.8)"
        playerScissors.classList.replace("item-highlight", "active")
        // playerScissors.style.borderRadius = "12px"
        this.CpuChoose()
        console.log("player adalah " + this.playerHand)
        console.log("cpu adalah " + this.cpuHand)
        this.playGame()
    }

    CpuChoose() {
        let randomNumber = Math.floor(Math.random() * 3) + 1
        switch (randomNumber) {
            case 1:
                this.cpuHand = "batu"
                cpuRock.style.backgroundColor = "rgba(255,255,255, 0.8)"
                cpuRock.style.borderRadius = "12px"
                break
            case 2:
                this.cpuHand = "kertas"
                cpuPaper.style.backgroundColor = "rgba(255,255,255, 0.8)"
                cpuPaper.style.borderRadius = "12px"
                break
            case 3:
                this.cpuHand = "gunting"
                cpuScissors.style.backgroundColor = "rgba(255,255,255, 0.8)"
                cpuScissors.style.borderRadius = "12px"
                break
        }

    }

    playGame() {
        switch (this.playerHand) {
            case this.cpuHand:
                indicator.textContent = "DRAW"
                indicator.style.backgroundColor = "#035B0C"
                indicator.style.color = "white"
                indicator.style.transform = 'rotate(-30deg)'
                indicator.style.fontSize = "2em"
                break
            case 'batu':
                switch (this.cpuHand) {
                    case 'kertas':
                        indicator.textContent = "COM WINS"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.backgroundColor = "#BD0000"
                        indicator.style.color = "white"
                        indicator.style.fontSize = "2em"
                        break
                    case 'gunting':
                        indicator.textContent = "PLAYER 1 WINS"
                        indicator.style.backgroundColor = "#4C9654"
                        indicator.style.color = "white"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.fontSize = "2em"
                        break
                }
                break
            case 'kertas':
                switch (this.cpuHand) {
                    case 'gunting':
                        indicator.textContent = "COM WINS"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.backgroundColor = "#BD0000"
                        indicator.style.color = "white"
                        indicator.style.fontSize = "2em"
                        break
                    case 'batu':
                        indicator.textContent = "PLAYER 1 WINS"
                        indicator.style.backgroundColor = "#4C9654"
                        indicator.style.color = "white"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.fontSize = "2em"
                        break
                }
                break
            case 'gunting':
                switch (this.cpuHand) {
                    case 'batu':
                        indicator.textContent = "COM WINS"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.backgroundColor = "#BD0000"
                        indicator.style.color = "white"
                        indicator.style.fontSize = "2em"
                        break
                    case 'kertas':
                        indicator.textContent = "PLAYER 1 WINS"
                        indicator.style.transform = 'rotate(-30deg)'
                        indicator.style.backgroundColor = "#4C9654"
                        indicator.style.color = "white"
                        indicator.style.fontSize = "2em"
                        break
                }
                break
        }

        // disable all buttons
        playerRock.disabled = true
        playerPaper.disabled = true
        playerScissors.disabled = true


    }

}


const playerRock = document.getElementById('player-rock')
const playerPaper = document.getElementById('player-paper')
const playerScissors = document.getElementById('player-scissors')

const cpuRock = document.getElementById('cpu-rock')
const cpuPaper = document.getElementById('cpu-paper')
const cpuScissors = document.getElementById('cpu-scissors')

let indicator = document.getElementById('indicator')
let resetButton = document.getElementById('reset-button')



resetButton.addEventListener("click", resetGame)


// reset game function
function resetGame() {
    playerRock.disabled = false
    playerPaper.disabled = false
    playerScissors.disabled = false
    indicator.textContent = "VS"
    indicator.style.transform = 'rotate(0)'
    indicator.style.fontSize = "8rem"
    indicator.style.color = "#BD0000"
    indicator.style.backgroundColor = "rgba(255,255,255, 0)"
    // playerRock.style.backgroundColor = "rgba(255,255,255, 0)"
    // playerPaper.style.backgroundColor = "rgba(255,255,255, 0)"
    // playerScissors.style.backgroundColor = "rgba(255,255,255, 0)"
    playerRock.classList.replace('active', "item-highlight")
    playerPaper.classList.replace('active', "item-highlight")
    playerScissors.classList.replace('active', "item-highlight")
    cpuRock.style.backgroundColor = "rgba(255,255,255, 0)"
    cpuPaper.style.backgroundColor = "rgba(255,255,255, 0)"
    cpuScissors.style.backgroundColor = "rgba(255,255,255, 0)"

}


rock = new game("batu")
paper = new game("kertas")
scissors = new game("gunting")
