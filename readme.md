20210324 Update

- db functions

  - db 'challenge7'
  - tables:
    -user
    -user_biodata

- API endpoints

  - get / (index.ejs)
  - get /login
  - get /register
  - get /dashboard
  - get /game
  - post /login
  - post /register

- issues:
  - http post /register is working using request.http, but not working from register.ejs
  - user authentication function is skipped (not working)
    - email check not working
    - duplication check not working
