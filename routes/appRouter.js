const { Router} = require('express');
const appController = require('../controllers/appController');
const router = Router();
let user = require('../models/user');

router.get('/', appController.index_get);
router.get('/register', appController.register_get);
router.post('/register', appController.register_post);
router.get('/dashboard', appController.dashboard_get);
router.get('/login', appController.login_get);
router.get('/game', appController.game_get)

module.exports = router;