'use strict';

const { isEmail } = require('validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


// create token
const maxAge = 3 * 24 * 60 * 60 //3dx24hx60mx60s
const createToken = (id) => {
    const secret = 'String Rahasia BINAR'
    return jwt.sign({id}, secret , {
        expiresIn: maxAge
    })
}


const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    toJSON(){
      return { ...this.get(), id: undefined}
    }
  };
  user.init({
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      isEmail: {msg: "Incorrect email"}
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      min: 4
    }
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};